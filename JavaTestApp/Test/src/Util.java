import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util{
  public static String convertStringToTimestamp(String str_date) {
    try {
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sssZ");
    	SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	Date d =  sdf.parse(str_date);
    	String formattedTime = output.format(d);
    	return formattedTime;
    } catch (ParseException e) {
      System.out.println("Exception :" + e);
      return null;
    }
  }
}