import java.sql.Timestamp;


public class TestClass {
	
	 private static double roundUp(double amount) {
	        double roundAmt = Math.round(amount * 100);
	        roundAmt = roundAmt / 100;

	        return roundAmt;
	  }
	 
	 
		/*if(rateSchedule!=null && rateSchedule.getHourlyRate()!=null) {
		minHours = rateSchedule.getHourlyRate().getMinTime();
	}
	else {
		minHours = new Double(2);
	}*/
	 private static Long calculateServiceTime(Timestamp dropoffTime, Timestamp pickupTime, Long incrementValue) {
			long tempTime =	(long) Math.max(pickupTime.getTime(), 0.00);
			tempTime = (long) Math.max(dropoffTime.getTime() - tempTime,0.00);	
			long reminderSeconds = Long.valueOf(Math.round(((tempTime % 60)*100/100))).longValue();	
		    long roundingFactor = reminderSeconds==0?0:reminderSeconds%incrementValue.longValue()==0?reminderSeconds/incrementValue.longValue():reminderSeconds/incrementValue.longValue()+1;
			long seconds = (tempTime/60)*60 + (roundingFactor)*incrementValue.longValue();
	     	Long serviceTime = Long.valueOf((long) (((Math.floor(Long.valueOf(seconds/60L).doubleValue())*100 + Math.round(((seconds % 60)*100/60)))/100)*60));
		    serviceTime=	(long) (0.0167 * (serviceTime * 0.001));
		    if(reminderSeconds > 30){
		    	serviceTime = serviceTime.longValue()+1;
		    }	
		 return serviceTime;
		}

		private static Long calculateServiceTime(Timestamp dropoffTime, Timestamp pickupTime) {
			long tempTimeMillis =	(long) Math.max(pickupTime.getTime(), 0.00);
			tempTimeMillis = (long) Math.max(dropoffTime.getTime() - tempTimeMillis,0.00);
		Long	tempTimeSeconds=tempTimeMillis/1000;
		System.out.println("Actul trip duration in minutes ::"+tempTimeSeconds/60);
			long reminderSeconds = Long.valueOf(Math.round(((tempTimeSeconds % (60))*100/100))).longValue();
			System.out.println("Seconds considered after changing minutes::"+reminderSeconds);		
		//long roundingFactor = reminderSeconds==0?0:reminderSeconds%incrementValue.longValue()==0?reminderSeconds/incrementValue.longValue():reminderSeconds/incrementValue.longValue()+1;
		//	long seconds = (tempTime/60)*60 + (roundingFactor)*incrementValue.longValue();
		//		Long serviceTime = Long.valueOf((long) (Math.floor(Long.valueOf(minutes/60L).doubleValue())));
    //	Long serviceTime = Long.valueOf((long) (((Math.floor(Long.valueOf(seconds/60L).doubleValue())*100 + Math.round(((seconds % 60)*100/60)))/100)*60));
    	Long	serviceTime=	(long) (0.0167 * (tempTimeMillis * 0.001));
    	if(reminderSeconds>=30){
    		serviceTime = serviceTime.longValue()+1;
    		System.out.println("Changed minutes after rounding ::"+serviceTime);
		}	
    	return serviceTime;
		}
		
		
		
		private static Long calculateServiceTimeNew(Timestamp actualDropoffTime, Timestamp lastUpdatedPickupTime) {
			long tempTimeMillis =	(long) Math.max(lastUpdatedPickupTime.getTime(), 0.00);
			tempTimeMillis = (long) Math.max(actualDropoffTime.getTime() - tempTimeMillis,0.00);
			/*Long	tempTimeSeconds=tempTimeMillis/1000;
		System.out.println("Actul trip duration in minutes ::"+tempTimeSeconds/60);
			long reminderSeconds = Long.valueOf(Math.round(((tempTimeSeconds % (60))*100/100))).longValue();
			System.out.println("Seconds considered after changing minutes::"+reminderSeconds);		
		//long roundingFactor = reminderSeconds==0?0:reminderSeconds%incrementValue.longValue()==0?reminderSeconds/incrementValue.longValue():reminderSeconds/incrementValue.longValue()+1;
		//	long seconds = (tempTime/60)*60 + (roundingFactor)*incrementValue.longValue();
		//		Long serviceTime = Long.valueOf((long) (Math.floor(Long.valueOf(minutes/60L).doubleValue())));
    //	Long serviceTime = Long.valueOf((long) (((Math.floor(Long.valueOf(seconds/60L).doubleValue())*100 + Math.round(((seconds % 60)*100/60)))/100)*60));
    	Long	serviceTime=	(long) (0.0167 * (tempTimeMillis * 0.001));
    	if(reminderSeconds>=30){
    		serviceTime = serviceTime.longValue()+1;
    		System.out.println("Changed minutes after rounding ::"+serviceTime);
		}*/	
		Double 	serviceTime =(double) round(0.0167 * (tempTimeMillis * 0.001),3);
		System.out.println("minutes:: "+serviceTime);
		String serviceTimeString = serviceTime.toString();
		String[] serviceTimeSeconds=serviceTimeString.split("[.]");
		System.out.println("remainder"+serviceTimeSeconds[1]);
		Long remainderSeconds = Long.valueOf(serviceTimeSeconds[1]);
		Long actualMinutes = Long.valueOf(serviceTimeSeconds[0]);
		if(remainderSeconds>=30){
			actualMinutes = actualMinutes+1;
		}
    	return actualMinutes;
		}
		public static double round(double val, int places) {
		    long factor = (long) Math.pow(10, places);

		    // Shift the decimal the correct number of places
		    // to the right.
		    val = val * factor;

		    // Round to the nearest integer.
		    long tmp = Math.round(val);

		    // Shift the decimal the correct number of places
		    // back to the left.
		    return (double) tmp / factor;
		  }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//
		String time1=Util.convertStringToTimestamp("2016-01-28T08:46:20+0400");
      //  String time =Util.convertStringToTimestamp("2016-01-28T08:01:53-0500");
		   String time =Util.convertStringToTimestamp("2016-01-28T08:46:20-0500Z");
        long minutes =0;		

Timestamp pickUpTm = null;
Timestamp dropOffTm = null;

if (null != time && time1!=null) {
	try {
		pickUpTm = Timestamp.valueOf(time);
		dropOffTm = Timestamp.valueOf(time1);
		System.out.println("drop off time"+dropOffTm);
		System.out.println("pick up time"+pickUpTm);
		/*long timeInMillis =ts1.getTime()-ts.getTime();
		minutes = (long) (0.0167 * (timeInMillis * 0.001));*/
	   } catch (Exception e) {
		
	}
}

	/*boolean flag=false;
		while(!flag){
			System.out.println(Calendar.getInstance().getTime().toString());
				
		}
	}*/
/*System.out.println("minute trip duration:::  "+minutes);
double timeDiff = (double) minutes / 60;
timeDiff = roundUp(timeDiff);
timeDiff = timeDiff*60;*/
minutes = calculateServiceTime(dropOffTm,pickUpTm);
System.out.println("minute trip duration:::  "+minutes);
	}

}

//14:71:60
//14:40:50

//00:31:10
/*
 2016-01-28T14:40:47-0500
2016-01-28T15:05:36-0500
 */
 