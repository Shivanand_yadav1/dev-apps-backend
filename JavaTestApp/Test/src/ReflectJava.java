import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ReflectJava {

    private static final String CONSTANT = "You can not change me, I'm a constant expression!";
    private static final Integer NOT_SO_CONSTANT = 10;
 
    public static String getConstant()
    {
        return CONSTANT;
    }
 
    public static String getConstantReflection()
    {
        try {
            final Field fld = ReflectJava.class.getDeclaredField( "CONSTANT" );
            return (String) fld.get( null );
        } catch (NoSuchFieldException e) {
            return null;
        } catch (IllegalAccessException e) {
            return null;
        }
    }
 
    public static int getNotSoConstant()
    {
        return NOT_SO_CONSTANT;
    }
    public static void main(String[] args) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
    	
    	SecurityManager sm = System.getSecurityManager();
    	sm.getSecurityContext();
    	Field field = ReflectJava.class.getDeclaredField( "CONSTANT" );
        field.setAccessible( true );

        //'modifiers' - it is a field of a class called 'Field'. Make it accessible and remove
        //'final' modifier for our 'CONSTANT' field
        Field modifiersField = Field.class.getDeclaredField( "modifiers" );
        modifiersField.setAccessible( true );
		modifiersField.setInt( field, field.getModifiers() & ~Modifier.FINAL );
		

        //it updates a field, but it was already inlined during compilation...
   
			field.set( null, "I was updated!" );
		
        //this method call will return an inlined value
        System.out.println( ReflectJava.getConstant() );
        //the only way to actually see an updated value is again via reflection
        System.out.println( ReflectJava.getConstantReflection() );
    }
    {
        //now try to update not constant expression type field
        Field field = null;
		try {
			field = ReflectJava.class.getDeclaredField( "NOT_SO_CONSTANT" );
		} catch (SecurityException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (NoSuchFieldException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

        field.setAccessible( true );

        //'modifiers' - it is a field of a class called 'Field'. Make it accessible and remove
        //'final' modifier for our 'CONSTANT' field
        Field modifiersField = null;
		try {
			modifiersField = Field.class.getDeclaredField( "modifiers" );
		} catch (SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchFieldException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        modifiersField.setAccessible( true );
        try {
			modifiersField.setInt( field, field.getModifiers() & ~Modifier.FINAL );
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        //this update should actually work
        try {
			field.set( null, -20 );
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        System.out.println( ReflectJava.getNotSoConstant() );
    }

    }
