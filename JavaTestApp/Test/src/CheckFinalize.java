// Package is different 
public class CheckFinalize{
         protected void finalize(){  //can't be declared protected if 
                 //it is public in Object class
                System.out.println("Check Finalize");
                //code to close connection for our class
        }
         public void update(){
                // code to update data at connection
        }
} 


class CallFinalize{
        public static void main(String[] args){
               CheckFinalize cf = new CheckFinalize();
               cf.finalize();
               cf.update();  // will throw IOException as connection is closed
       }
}





 class CheckFinalize1{
         protected void finalize(){  //declared protected as it 
                //is protected in Object class
                System.out.println("Check Finalize");
                //code to close connection for our class
         }
         public void update(){
                // code to update data at connection
        }
} 



 class CallFinalize1{
        public static void main(String[] args){
               CheckFinalize cf = new CheckFinalize();
               cf.finalize(); 
               cf.update();  // No Exception
       }
}




