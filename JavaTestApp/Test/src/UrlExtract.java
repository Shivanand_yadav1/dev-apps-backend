import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class UrlExtract {

	/**
	 * @param args
	 */
	
	public static void extractUrl(URL url){
	
		//
		String query = url.getQuery();
		Map<String, String> map = getQueryMap(query);
		Set<String> keys = map.keySet();
		for (String key : keys)
		{
		   System.out.println("Name=" + key);
		   System.out.println("Value=" + map.get(key));
		}
	}
	


	public static Map<String, String> getQueryMap(String query)
	{
	    String[] params = query.split("&");
	    Map<String, String> map = new HashMap<String, String>();
	    for (String param : params)
	    {
	        String name = param.split("=")[0];
	        String value = param.split("=")[1];
	        map.put(name, value);
	    }
	    return map;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			URL url = new URL("http://localhost:8082/TestData/rest/chaufeurAssign?ServiceCityId=11&latitude=3300&longitude=111&tripId=1234&channelType=carey");
			extractUrl(url);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

}
