package com.util.concurrent.delayqueue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;


public class DelayQueueTest {

	/**
	 * @param args
	 */
		public static void main(String[] args) {
			long initialTime = System.currentTimeMillis();
			final BlockingQueue<DelayElement> queue = new DelayQueue<DelayElement>();
		//	final BlockingQueue queue = new DelayQueue();

			DelayQueueProducer queueProducer = new DelayQueueProducer(queue);
			Thread t =new Thread(queueProducer);
             t.start();
			DelayQueueConsumer queueConsumer1 = new DelayQueueConsumer(queue);
			  new Thread(queueConsumer1).start();

			DelayQueueConsumer queueConsumer2 = new DelayQueueConsumer(queue);
			new Thread(queueConsumer2).start();
			DelayQueueConsumer queueConsumer3 = new DelayQueueConsumer(queue);
			new Thread(queueConsumer3).start();

			DelayQueueConsumer queueConsumer4 = new DelayQueueConsumer(queue);
			new Thread(queueConsumer4).start();
			long endTime = System.currentTimeMillis();
		}

}
