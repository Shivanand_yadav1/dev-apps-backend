package com.util.concurrent.priorityqueue;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 *Creates a PriorityBlockingQueue with the default initial capacity (11) that orders 
 * its elements according to their natural ordering.
 *
 */
public class PriorityBlockingQueueTest {
    public static void main(String[] args) {
        final String[] names =
                {"carol", "alice", "malory", "bob", "alex", "jacobs","3","7","ij","ko","knk","mjo","0","8","7"};
        System.out.println("insrted objects in queue in order are..."+Arrays.deepToString(names));
        @SuppressWarnings("rawtypes")
		final BlockingQueue queue = new PriorityBlockingQueue();

        new Thread(new Runnable() {
            @SuppressWarnings("unchecked")
			@Override
            public void run() {
                for (int i = 0; i < names.length; i++) {
                    try {
                    //	System.out.println(names[i]+"   inserted into queue......");
                        queue.put(names[i]);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, "Producer").start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < names.length; i++) {
                        System.out.println(queue.take()+":::Consumed ");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();	
                }
            }
        }, "Consumer").start();
    }
}