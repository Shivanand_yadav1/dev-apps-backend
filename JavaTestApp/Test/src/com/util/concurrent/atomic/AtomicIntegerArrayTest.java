package com.util.concurrent.atomic;

import java.util.concurrent.atomic.AtomicIntegerArray;

public class AtomicIntegerArrayTest {
	AtomicIntegerArray atomicArray= new AtomicIntegerArray(10);
	class AddThread implements Runnable{

		//s@Override
		public void run() {
		//add the value 2 at index 1 
		int current = atomicArray.addAndGet(1,15);
		  System.out.println("current value at position 1::"+current);
		//at index 0, if current value is 15, then 5 is added to current value
			atomicArray.compareAndSet(1,15,5);
			System.out.println("current value at 1 after compareAndset  is ::"+atomicArray.get(1));
		}
	}
	
	class SubThread implements Runnable{

		//@Override
		public void run() {
			
			//decrements by 1 from current value at index 1 
		int afterDecrement = 	atomicArray.decrementAndGet(1);
			System.out.println("afterDecrements :: "+afterDecrement);
	
			//weakly compare the first args to current value and sets
		//	atomicArray.weakCompareAndSet(1,14,20);
			System.out.println("current value at 1 position ::"+atomicArray.get(1));
		}
		
	}
	
   public static void main(String[]args){
	   new Thread(new AtomicIntegerArrayTest().new AddThread()).start();
	   new Thread(new AtomicIntegerArrayTest().new SubThread()).start();
   }
}
