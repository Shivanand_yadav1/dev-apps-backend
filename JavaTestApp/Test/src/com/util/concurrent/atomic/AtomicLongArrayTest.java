package com.util.concurrent.atomic;

import java.util.concurrent.atomic.AtomicLongArray;

public class AtomicLongArrayTest {
	AtomicLongArray atomicLongArray= new AtomicLongArray (10);
	class AddThread implements Runnable{

		@Override
		public void run() {
		//add the value 2 at index 1 
		atomicLongArray.addAndGet(1,15);
		
		//at index 0, if current value is 15, then 5 is added to current value
		atomicLongArray.compareAndSet(1,15,5);
		
			System.out.println(atomicLongArray.get(1));
		}
		
	}
	
	class SubThread implements Runnable{

		@Override
		public void run() {
			
			//decrements by 1 from current value at index 1 
			atomicLongArray.decrementAndGet(1);
			
			//weakly compare the first args to current value and sets
			atomicLongArray.weakCompareAndSet(1,14,20);
			System.out.println(atomicLongArray.get(1));
		}
		
	}
	
   public static void main(String... args){
	   new Thread(new AtomicLongArrayTest().new AddThread()).start();
	   new Thread(new AtomicLongArrayTest().new SubThread()).start();
   }
}
