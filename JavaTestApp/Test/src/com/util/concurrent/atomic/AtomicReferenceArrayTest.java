package com.util.concurrent.atomic;

import java.util.concurrent.atomic.AtomicReferenceArray;

public class AtomicReferenceArrayTest {
	AtomicReferenceArray<String> atomicReferenceArray= new AtomicReferenceArray<String>(10);
	
	class AddThread implements Runnable{

		@Override
		public void run() {
			
		//sets value at the index 0
		atomicReferenceArray.set(0,"ref");
		
		//at index 0, if current reference is "ref" then it changes as newref 
		atomicReferenceArray.compareAndSet(0,"ref","newref");
		
		//at index 0, if current value is newref, then it is sets as finalref
		atomicReferenceArray.weakCompareAndSet(0,"newref","finalref");
		
		System.out.println(atomicReferenceArray.get(0));
		
		}
		
	}
	
   public static void main(String... args){
	   new Thread(new AtomicReferenceArrayTest().new AddThread()).start();
	   new Thread(new AtomicReferenceArrayTest().new AddThread()).start();
   }
}
