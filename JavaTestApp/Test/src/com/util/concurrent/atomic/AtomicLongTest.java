package com.util.concurrent.atomic;

import java.util.concurrent.atomic.AtomicLong;


class MyRunnable implements Runnable{
    
    public void run(){
           for(int i=0;i<2;i++){
                  System.out.println("ThreadName="+Thread.currentThread().getName()
                               +" sharedAtomicLong value > "+
                               AtomicLongTest.sharedAtomicLong.incrementAndGet());
           }          
           
    }
}
 
 
/** Copyright (c), AnkitMittal JavaMadeSoEasy.com */
/**
 * Main class
 */
public class AtomicLongTest {
    
    //Create a new AtomicLong and is initialized to 0.
    static AtomicLong sharedAtomicLong =new AtomicLong();
    
    public static void main(String...args) throws InterruptedException{
           MyRunnable runnable=new MyRunnable();
           Thread thread1=new Thread(runnable,"Thread-1");
           Thread thread2=new Thread(runnable,"Thread-2");
           thread1.start();
           thread2.start();
           
           Thread.sleep(1000); //delay to ensure Thread-1 and Thread-2 finish
           System.out.println("After completion of both threads, "
                        + "sharedAtomicLong = "+sharedAtomicLong);
    }
}
