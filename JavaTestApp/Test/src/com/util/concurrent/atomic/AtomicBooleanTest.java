package com.util.concurrent.atomic;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * used to boolean in multithreaded env.
 *
 */
public class AtomicBooleanTest {
	AtomicBoolean ab= new AtomicBoolean(true);
	boolean initialStatus = false;
	boolean newStatus = true;
	class A implements Runnable{

		//@Override
		public void run() {
			ab.compareAndSet(false, true);
			System.out.println("In A ....."+ab.get());
		}
		
	}
	
	Runnable r1=() -> System.out.println("Using lamda exprssion...................");
	
	class B implements Runnable{

		//@Override
		public void run() {
			ab.compareAndSet(true, false);
			System.out.println("In B...."+ab.get());
		}
		
	}
	
   public static void main(String[] args){
	   new Thread(new AtomicBooleanTest().new A()).start();
	   new Thread(new AtomicBooleanTest().new B()).start();
   }
}
