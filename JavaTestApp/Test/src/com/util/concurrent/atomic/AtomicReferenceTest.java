package com.util.concurrent.atomic;

import java.util.concurrent.atomic.AtomicReference;

import com.test.sorting.A;

public class AtomicReferenceTest {
	//AtomicReference<String> ar= new AtomicReference<String>("ref");
	AtomicReference<A> ar= new AtomicReference<A>();
	A a1 = new A(1, "shiva", "UN", 10);
	A a2 = new A(8, "Amit", "UN", 8);
	/*class AddThread implements Runnable{

		@Override
		public void run() {
			
		//if current reference is "ref" then it changes as newref 
		ar.compareAndSet("ref","newref");
		
		// sets the new reference without any check  
		ar.set("reference");
		
		//sets new value and return the old value
		String s= ar.getAndSet("reference1");
		
		System.out.println(s);
		
		}
		
	}*/
	class AddThread implements Runnable{

		@Override
		public void run() {
			
		//if current reference is "ref" then it changes as newref 
		ar.compareAndSet(a1,a2);
		
		// sets the new reference without any check  
		ar.set(a2);
		
		//sets new value and return the old value
		A updatedA= ar.getAndSet(a2);
		
		System.out.println("updated name ::"+updatedA.getName());
		
		}
		
	}
   public static void main(String... args){
	   new Thread(new AtomicReferenceTest().new AddThread()).start();
	   new Thread(new AtomicReferenceTest().new AddThread()).start();
   }
}
