package com.util.concurrent.locks;

import java.util.concurrent.locks.ReentrantLock;


/*ReentrantLock is mutual exclusive lock, similar to implicit locking provided by synchronized keyword in Java,
with extended feature like fairness, which can be used to provide lock to longest waiting thread. 
Lock is acquired by lock() method and held by Thread until a call to unlock() method.*/

public class ReentntrantLocks {
    public static void main(String[] args) {
        ReentrantLock rLock = new ReentrantLock();
        Thread t1 = new Thread(new Display("Thread-A", rLock));
     // t1.setPriority(1);//java.lang.IllegalArgumentException
        Thread t2 = new Thread(new Display("Thread-B", rLock));
        t1.setPriority(10);//java.lang.IllegalArgumentException
        System.out.println("starting threads ");
        t1.start();
        t2.start();
    }
}

class Display implements Runnable {
    private String threadName;
    ReentrantLock lock;
    Display(String threadName, ReentrantLock lock){
        this.threadName = threadName;
        this.lock = lock;
    }
    @Override
    public void run() {
        System.out.println("In Display run method, thread " + threadName + " is waiting to get lock");
        
     try {
            lock.lock();
           
            System.out.println("Total no of waiting threads are :::"+lock.getQueueLength());
            System.out.println( threadName + " has got lock");
            if(lock.isHeldByCurrentThread()){
            	System.out.println(threadName+"....is having lock...... ");
            }
            methodA();
     }
            catch(Exception e){
            	System.out.println("Error ocurred while thread locking..."+e);
            
             // calling unlock
             } 
     finally{
            lock.unlock();
        }
        
    }
    
    public void methodA(){
        System.out.println("In Display methodA, thread " + threadName + " is waiting to get lock");
    try {
            lock.lock();
            System.out.println("total no of waiting threads are :::"+lock.getQueueLength());
            System.out.println(threadName + " has got lock");
            System.out.println("Count of locks held by thread " + threadName + " - " + lock.getHoldCount());
            if(lock.isHeldByCurrentThread()){
            	System.out.println(threadName+"....is having lock...... ");
            }
            // calling unlock
         } 
    catch(Exception e){
    	System.out.println("error ocurred while thread locking..."+e);
    }
    finally{
            lock.unlock();
        }
    }    
}
