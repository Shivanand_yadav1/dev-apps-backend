package com.util.concurrent.locks;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.LockSupport;

 /**
 Basic thread blocking primitives for creating locks and other synchronization classes. 
This class associates, with each thread that uses it, a permit (in the sense of the Semaphore class). 
A call to park will return immediately if the permit is available, 
consuming it in the process; otherwise it may block. 
A call to unpark makes the permit available, if it was not already available.
 (Unlike with Semaphores though, permits do not accumulate. There is at most one.) 

Methods park and unpark provide efficient means of blocking and unblocking threads that
 do not encounter the problems that cause the deprecated methods Thread.suspend and Thread.
 resume to be unusable for such purposes: Races between one thread invoking park and another 
 thread trying to unpark it will preserve liveness, due to the permit. 
 Additionally, park will return if the caller's thread was interrupted, 
 and timeout versions are supported. The park method may also return at any other time, 
 for "no reason", so in general must be invoked within a loop that rechecks conditions upon return. 
 In this sense park serves as an optimization of a "busy wait" that does not waste as much time spinning, 
 but must be paired with an unpark to be effective. 

The three forms of park each also support a blocker object parameter. 
This object is recorded while the thread is blocked to permit monitoring and
 diagnostic tools to identify the reasons that threads are blocked.
  (Such tools may access blockers using method getBlocker.) 
  The use of these forms rather than the original forms without this parameter is strongly encouraged. 
  The normal argument to supply as a blocker within a lock implementation is this. 
These methods are designed to be used as tools for creating higher-level synchronization utilities, 
and are not in themselves useful for most concurrency control applications. The park method is designed
 for use only in constructions of the form: 
while (!canProceed()) { ... LockSupport.park(this); }
where neither canProceed nor any other actions prior to the call to park entail locking or blocking.
 Because only one permit is associated with each thread, any intermediary uses of park could interfere with its intended effects. 
 */
class TestLockSupportTask {

	private AtomicBoolean atLocked = new AtomicBoolean(false);
	   @SuppressWarnings("rawtypes")
	private Queue waitingThreads = new ConcurrentLinkedQueue();
	   @SuppressWarnings("unchecked")
	public void lockRes() { 
	     Thread current = Thread.currentThread();
	     waitingThreads.add(current);
	     while (waitingThreads.peek() != current || 
	            !atLocked.compareAndSet(false, true)) { 
	    	 System.out.println("park thread thread......");
	    	 
	    	 //park() Disables the current thread for thread scheduling purposes unless the permit is available. 
	        LockSupport.park();
	     }
	     
	     //remove() : Retrieves and removes the head of this queue.
	     //This method differs from poll only in that it throws an exception if this queue is empty.
    	 System.out.println("removing thread.....");

    	 waitingThreads.remove();
	   }
	   public void unlockRes() {
	     atLocked.set(false);
	     
	     //unpark(thread) :: Makes available the permit for the given thread, if it was not already available. 
	     //If the thread was blocked on park then it will unblock.
	     //Otherwise, its next call to park is guaranteed not to block.
	     //This operation is not guaranteed to have any effect at all if the given thread has not been started.
    	 System.out.println("unpark  thread.....");
 
	     LockSupport.unpark((Thread) waitingThreads.peek());
	   } 
	 }
public class TestLockSupport{
	public static void main(String args[]){
		new TestLockSupportTask().lockRes();
		new TestLockSupportTask().unlockRes();
	}
}


