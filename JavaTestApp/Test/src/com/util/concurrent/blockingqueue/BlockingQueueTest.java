package com.util.concurrent.blockingqueue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


public class BlockingQueueTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		      //  BlockingQueue queue = new ArrayBlockingQueue(1024);
		BlockingQueue<Integer> queue = new LinkedBlockingQueue<Integer>(100);
		        Producer producer = new Producer(queue);
		        Consumer consumer = new Consumer(queue);

		        new Thread(producer).start();
		        new Thread(consumer).start();

		        try {
					Thread.sleep(4000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }


}
