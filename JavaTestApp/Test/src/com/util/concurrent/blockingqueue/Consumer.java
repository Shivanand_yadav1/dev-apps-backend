package com.util.concurrent.blockingqueue;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable{

    protected BlockingQueue<Integer> queue = null;

    public Consumer(BlockingQueue<Integer> queue) {
        this.queue = queue;
    }

    public void run() {
        try {
            System.out.println("Consuming :: "+queue.take());
            System.out.println("Consuming :: "+queue.take());
            System.out.println("Consuming :: "+queue.take());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
