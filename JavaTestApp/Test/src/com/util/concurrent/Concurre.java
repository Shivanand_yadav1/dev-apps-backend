package com.util.concurrent;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
public class Concurre {

	/**
	 * @param args
	 */
	
	static ConcurrentHashMap<String, String> getData(){
		System.out.println("**********ConcurrentHashMap***********");
		ConcurrentHashMap<String, String> cMap=new ConcurrentHashMap<String, String>();
		cMap.put("Shiva0", "Aaaaa");
		cMap.put("Shiva1", "Aaaaa");
		cMap.put("Shiva2", "Aaaaa");
		cMap.put("Shiva3", "Aaaaa");
		cMap.put("Shiva4", "Aaaaa");
		cMap.put("Shiva5", "Aaaaa");
		cMap.put("Shiva6", "Aaaaa");
		cMap.put("Shiva7", "Aaaaa");
		cMap.put("Shiva8", "Aaaaa");
		cMap.put("Shiva9", "Aaaaa");
		System.out.print("Shiva8 :: value.."+cMap.get("Shiva8"));
		return cMap;
	}
	
	
	static CopyOnWriteArrayList<String> getCopyOnwriteArrayList(){
		System.out.println("**********CopyOnWriteArrayList***********");
		CopyOnWriteArrayList<String> strList=new CopyOnWriteArrayList<String>();
		strList.add("a");
		strList.add("b");
		strList.add("c");
		strList.add("d");
		strList.add("e");
		strList.add("f");
		strList.add("g");
		strList.add("h");
		return strList;
	}
	
	static Set<String> getCopyonArrayset(){
		System.out.println("**********CopyOnWriteArraySet***********");
		CopyOnWriteArraySet<String> strList=new CopyOnWriteArraySet<String>();
		strList.add("a");
		strList.add("b");
		strList.add("c");
		strList.add("d");
		strList.add("e");
		strList.add("f");
		strList.add("g");
		strList.add("h");
		return strList;
	}
  /*static ConcurrentSkipListSet<String> getSkipListSet(){
	System.out.println("**********ConcurrentSkipListSet***********");
	ConcurrentSkipListSet<String> skipListset = new ConcurrentSkipListSet<String>();
	skipListset.add("a");
	skipListset.add("c");
	skipListset.add("b");

	skipListset.add("d");
	System.out.println("total set items::"+	skipListset.size());
	return skipListset;
   }
  */
  /**
 * Skip List::

A skip list is a type of data structure that allows fast search in an ordered sequence of elements.
 This is built using multiple sequence of layers.
  The lowest layer linking all the elements by a linked list and subsequent 
  layers above skips some elements in between the links. Thus the highest layer
   contains the least number of elements in the linked sequence.
 The elements that are to be skipped in between are chosen probabilistically.
 */
/**
 *A scalable concurrent ConcurrentNavigableMap implementation. 
 *The map is sorted according to the natural ordering of its keys,
 * or by a Comparator provided at map creation time, depending on which constructor is used. 

This class implements a concurrent variant of SkipLists providing expected average log(n)
 time cost for the containsKey, get, put and remove operations and their variants.
  Insertion, removal, update, and access operations safely execute concurrently by multiple threads. 
  Iterators are weakly consistent, returning elements reflecting the state of the map at some point at or 
  since the creation of the iterator.
   They do not throw ConcurrentModificationException, 
   and may proceed concurrently with other operations. 
   Ascending key ordered views and their iterators are faster than descending ones. 

All Map.Entry pairs returned by methods in this class and its views represent snapshots 
of mappings at the time they were produced. They do not support the Entry.setValue method. (Note however that it is possible to change mappings in the associated map using put, putIfAbsent, or replace, depending on exactly which effect you need.) 

 */
static ConcurrentSkipListMap<String,String> getSkipListMap(){
	System.out.println("**********ConcurrentSkipListMap***********");
	ConcurrentSkipListMap<String,String> skipListMap = new ConcurrentSkipListMap<String,String>();
	skipListMap.put("a","");
	skipListMap.put("c","");
	skipListMap.put("b","");
	skipListMap.put("d","");
	System.out.println("total set items::"+	skipListMap.size());	
	return skipListMap;
    }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*ConcurrentSkipListSet<String> skipSetData = getSkipListSet();
		for(String key:skipSetData){
			System.out.println("set key:::"+key);
			skipSetData.remove(key);
			 System.out.println("total items in the collections .........."+skipSetData.size());
		}
		
		ConcurrentSkipListMap<String,String>  skipMapData = getSkipListMap();
		for(String key:skipMapData.keySet()){
			System.out.println(" key:::"+key);
			skipMapData.remove(key);
			 System.out.println("total items in the collections .........."+skipMapData.size());
		}
	ConcurrentHashMap<String, String> cMap= getData();
		for(String key:cMap.keySet()){
			//System.out.println("added item as .........."+);
			 System.out.println("removing key .........."+key);
		 cMap.remove(key);
		 System.out.println("total items in the collections .........."+cMap.size());
		}*/
		
	//	Set<String> cSet= Collections.newSetFromMap(getData());
		CopyOnWriteArrayList<String> list = getCopyOnwriteArrayList();
		for(String str:list){
			System.out.println("string iterated:::::"+str);
			list.add("");
			list.remove(str);
		}
	}	
}
