package com.util.concurrent.Executor;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
 
 
class MyRunnable implements Runnable {
    @Override
    public void run() {
           System.out.println("MyRunnable's run()");
           
    }
}
 
 
public class SubmitRunnableExample {
  private static final int NTHREDS = 10;
 
  public static void main(String[] args) throws InterruptedException, ExecutionException {
  boolean mayInterruptIfRunning =false;
    ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
 
    Future<Integer> futureInteger=executor.submit(new MyRunnable(), 1);
    System.out.println("futureInteger.get() > "+futureInteger.get());
     
    Future<?> future=executor.submit(new MyRunnable());
    
    //future.get()
  //Waits if necessary for the computation to complete, and then retrieves its result.
    System.out.println("future.get() > "+future.get());
    
    //future.cancel(mayInterruptIfRunning)
    /*Attempts to cancel execution of this task. This attempt will fail if the task has already completed,
    has already been cancelled, or could not be cancelled for some other reason. If successful,
    and this task has not started when cancel is called, this task should never run. If the task has already started,
    then the mayInterruptIfRunning parameter determines whether the thread executing 
    this task should be interrupted in an attempt to stop the task. */
    System.out.println("future.get() > "+future.cancel(mayInterruptIfRunning));
    
    //future.isCancelled()
    //Returns true if this task was cancelled before it completed normally.
    System.out.println("is cancelled :: > "+future.isCancelled());
    
    //future.isDone()
    //Returns true if this task completed. Completion may be due to normal termination,
    //an exception, or cancellation -- in all of these cases, this method will return true
    System.out.println("is task completed :: > "+future.isDone());
    
    //executor.shutdownNow()::
    //Attempts to stop all actively executing tasks, halts the processing of waiting tasks, 
    //and returns a list of the tasks that were awaiting execution. 
//There are no guarantees beyond best-effort attempts to stop processing actively executing tasks. 
//For example, typical implementations will cancel via Thread.interrupt,
    //so any task that fails to respond to interrupts may never terminate.
   executor.shutdownNow();
  }
}
