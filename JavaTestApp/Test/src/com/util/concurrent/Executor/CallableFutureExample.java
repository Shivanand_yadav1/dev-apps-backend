package com.util.concurrent.Executor;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
 
 
class SumIntegerCallable implements Callable<Integer> {
 
    Integer n;
 
    SumIntegerCallable(Integer n) {
           this.n = n;
    }
 
    @Override
    public Integer call() throws Exception {
           Integer sum = 0;
           for (int i = 0; i <= n; i++) {
                  sum += i;
           }
           return sum;
    }
 
}
 
 
class SquareDoubleCallable implements Callable<Double> {
 
    Double n;
 
    SquareDoubleCallable(Double n) {
           this.n = n;
    }
 
    @Override
    public Double call() throws Exception {
           return n*n;
    }
 
}
 
 
/*A Future represents the result of an asynchronous computation. 
Methods are provided to check if the computation is complete,
to wait for its completion, and to retrieve the result of the computation.
The result can only be retrieved using method get when the computation has completed, 
blocking if necessary until it is ready. Cancellation is performed by the cancel method. 
Additional methods are provided to determine if the task completed normally or was cancelled.
Once a computation has completed, the computation cannot be cancelled. 
If you would like to use a Future for the sake of cancellability but not provide a usable result, 
you can declare types of the form Future<?> and return null as a result of the underlying task. */
 
public class CallableFutureExample {
  private static final int NTHREDS = 10;
 
  public static void main(String[] args) throws InterruptedException, ExecutionException {
 
    ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
    Future<Integer> futureInteger=executor.submit(new SumIntegerCallable(4));
    Future<Double> futureDouble=executor.submit(new SquareDoubleCallable(2.2));
   
    System.out.println("SumIntegerCallable has returned > "+futureInteger.get());
    System.out.println("SquareDoubleCallable has returned > "+futureDouble.get());
  
    //executor.shutdown()::
    //Initiates an orderly shutdown in which previously submitted tasks are executed,
    //but no new tasks will be accepted. Invocation has no additional effect if already shut down.
    executor.shutdown();
  
  //Attempts to stop all actively executing tasks, halts the processing of waiting tasks,
  //and returns a list of the tasks that were awaiting execution. 
  // This method does not wait for actively executing tasks to terminate. Use awaitTermination to do that. 
  //There are no guarantees beyond best-effort attempts to stop processing actively executing tasks.
  // For example, typical implementations will cancel via Thread.interrupt,
  //so any task that fails to respond to interrupts may never terminate.

   List<Runnable> r =executor.shutdownNow();
  }
}
