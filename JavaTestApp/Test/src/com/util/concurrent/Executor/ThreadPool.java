package com.util.concurrent.Executor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
public class ThreadPool {
	 
    public static void main(String args[]) {
    	
       ExecutorService executerService = Executors.newFixedThreadPool(10);
       for (int i =0; i<10; i++){
    	   executerService.submit(new MyTask(i));
       }
       executerService.shutdown();
    }  
}
 
final class MyTask implements Runnable{
    private int taskId;
    static AtomicInteger i;
    public MyTask(int id){
        this.taskId = id;
    }
   
    @Override
    public void run() {
      //  System.out.println("Task ID : " + this.taskId +" performed by "
     //                      + Thread.currentThread().getName());
        System.out.println("Task ID : " + this.taskId +" performed by ::"
                + Thread.currentThread().getName());
    }  
}