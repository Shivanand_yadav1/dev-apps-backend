package com.cloneImpl;

import java.util.Collections;

public class PupilVODeep implements Cloneable {

	// Contained object
	private SubjectVO subj;
	private String name;

	/**
	 * @return the subj
	 */
	public SubjectVO getSubj() {
		return subj;
	}

	/**
	 * @param subj
	 * the subj to set
	 */
	public void setSubj(SubjectVO subj) {
		this.subj = subj;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public PupilVODeep(String name, String sub) {
		this.name = name;
		this.subj = new SubjectVO(sub);
	}
@Override
	public Object clone() {
		// deep copy
		PupilVODeep pupil = new PupilVODeep(name, subj.getName());
		//System.
		return pupil;
	}

}
