package com.cloneImpl;

public class CloneClass implements Cloneable{

	/**
	 * @param args
	 */
	
	private String name;
	public void setName(String name) {
		this.name = name;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getName() {
		return name;
	}
	public String getAddress() {
		return address;
	}
	private String address;
	CloneClass(String name, String address){
		this.name=name;
		this.address=address;
	}
	public Object Clone() throws CloneNotSupportedException {
		return super.clone();
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CloneClass obj=new CloneClass("shiva","noida");
		try {
			CloneClass obj2=(CloneClass) obj.Clone();
			System.out.println("obj1 name::"+obj2.getName());
			System.out.println("obj1 addrss::"+obj2.getAddress());
			System.out.println("obj1 hashcode::"+obj2.hashCode());
			System.out.println("obj1 and obj equality by ==::"+(obj2==obj));
			System.out.println("obj1 and obj equality by equals::"+obj2.equals(obj));
            obj2.setAddress("delhi");
            obj2.setName("shiva2");
             System.out.println("obj2 name Aftre changing::"+obj2.getName());
 			 System.out.println("obj2 addrss After changing::"+obj2.getAddress());
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("obj name::"+obj.getName());
		System.out.println("obj addrss::"+obj.getAddress());
		System.out.println("obj hashcode::"+obj.hashCode());
		
	}
}
