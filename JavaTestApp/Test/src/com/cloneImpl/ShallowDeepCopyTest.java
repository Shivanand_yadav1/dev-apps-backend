package com.cloneImpl;

public class ShallowDeepCopyTest {

	public static void main(String[] args) {
		// Original Object
       System.out.println("####################################shallow cloning#####################");	
		
		PupilVO stud1 = new PupilVO("Johnathan", "Algebra");
		System.out.println("Original Object: " + stud1.getName() + " - "
				+ stud1.getSubj().getName());
		// Clone Object
		PupilVO clonedStud1 = (PupilVO) stud1.clone();
		System.out.println("Cloned Object: " + clonedStud1.getName() + " - "
				+ clonedStud1.getSubj().getName());
		
		//change values of fields.................
		stud1.setName("Daniel");
		stud1.getSubj().setName("Physics");
		System.out.println("Original Object after it is updated: "
				+ stud1.getName() + " - " + stud1.getSubj().getName());
		System.out.println("Cloned Object after updating original object: "
						+ clonedStud1.getName() + " - "
						+ clonedStud1.getSubj().getName());
		
		System.out.println("####################################Deep cloning#####################");
		PupilVODeep stud = new PupilVODeep("Johnathan", "Algebra");
		System.out.println("Original Object: " + stud.getName() + " - "
				+ stud.getSubj().getName());
		// Clone Object
		PupilVODeep clonedStud = (PupilVODeep) stud.clone();
		System.out.println("Cloned Object: " + clonedStud.getName() + " - "
				+ clonedStud.getSubj().getName());
		
		//change values of fields.................
		stud.setName("Daniel");
		stud.getSubj().setName("Physics");
		System.out.println("Original Object after it is updated: "
				+ stud.getName() + " - " + stud.getSubj().getName());
		System.out.println("Cloned Object after updating original object: "
						+ clonedStud.getName() + " - "
						+ clonedStud.getSubj().getName());
	}

}


