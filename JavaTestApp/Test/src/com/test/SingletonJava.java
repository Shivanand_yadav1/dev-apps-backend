package com.test;

public class SingletonJava {
	
	//by early initialization
private static  final SingletonJava obj=new SingletonJava();
	private SingletonJava(){		
	  }
	
	public static   SingletonJava getSigleTon(){
		return obj;
	}
	
	public Object Clone() throws CloneNotSupportedException{
		return obj;
	}
}
