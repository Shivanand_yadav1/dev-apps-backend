package com.test.collections;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
public class ArraysImpl {

	/**
	 * @param args
	 */
	public String[] strArr ={"a","b"};
	public String[] originalArr ={"a","b"};
	public String[] newstrArr ={"a","b"};
	public List<String> list =null;
	Comparator<String> cmp = null;
	public void getArrayUtils(){
		
		//get list for array
		list = Arrays.asList(strArr);
		
		//sort this array first
		Arrays.sort(strArr);
		
		//Now perform binary search for specified element in sorted array 
		 int foundAt =Arrays.binarySearch(strArr, "a");
		 System.out.println("a found at ::"+foundAt);
/*		 
		 Copies the specified array, truncating or padding with nulls (if necessary) 
		 so the copy has the specified length. For all indices that are valid in both
		 the original array and the copy, the two arrays will contain identical values. 
		 For any indices that are valid in the copy but not the original, the copy will contain null.
		 Such indices will exist if and only if the specified length is greater than that of the original array. 
		 The resulting array is of exactly the same class as the original array.*/
		 
		 System.out.println("original array elements are as follows ::"+originalArr);
		 newstrArr =  Arrays.copyOf(originalArr, 10);
		 
		 System.out.println("new array elements are as follows ::"+newstrArr);
		 //Arrays.copyOfRange(originalArr, 1, 4, String.class[]);
		 
		 //checks equality of elements of specified arrays
	/*	 Returns true if the two specified arrays are deeply equal to one another.
		 Unlike the equals(Object[], Object[]) method, this method is appropriate for
		 use with nested arrays of arbitrary depth. 

		 Two array references are considered deeply equal if both are null, or 
		 if they refer to arrays that contain the same number of elements and all corresponding pairs of
		 elements in the two arrays are deeply equal. */

		 Arrays.deepEquals(strArr, originalArr);
		 
		 Arrays.deepHashCode(strArr);

		 Arrays.deepToString(strArr);
		 
		 boolean yes =Arrays.equals(strArr, newstrArr);
		 System.out.println("yes+"+yes);
		 
		 Arrays.fill(strArr, "a");
		
		 Arrays.sort(strArr);
		
		 Arrays.sort(strArr, cmp);
		 
		 int fromIndex =1,toIndex =10;
		 Arrays.sort(strArr, fromIndex, toIndex);
		 Arrays.toString(strArr);
	}
	
}
