package com.test.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;

public class Collectionsimpl {

	/**
	 * @param args
	 */
	public List<String> list;
	public List<String> sourceList;
	public List<String> destinationList;
	public Deque<String> Dq;
	public Map<String,String> map;
	public Map<Entry,Boolean> stMap ;
	public SortedMap<String,String> smap;
	public Set<String> set;
	public SortedSet<String> sset;
	public Enumeration<String> en;
	static List<String> getStringList(){
		List<String> list = new ArrayList<String>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		list.add("f");
		return list;
	}
	
	
	/**
	 * Using Collections methods...
	 */
	void getUtility(){
	//empty list used for send serialization instead of returning null from a method.	
	List empty =	Collections.EMPTY_LIST;
	 Collections.emptyList();Collections.emptyMap();Collections.emptySet();
	//Collections.EMPTY_MAP,Collections.EMPTY_SET or 
	
	//add elements to existing collections 
	Collections.addAll(list, "x","y","z","");
	
	//Returns a view of a Deque as a Last-in-first-out (Lifo) Queue. 
	
	//Method add is mapped to push, remove is mapped to pop and so on.
	//This view can be useful when you would like to use a method requiring a Queue but you need Lifo ordering. 

   //Each method invocation on the queue returned by this method results in exactly one method invocation on the backing deque, with one exception. The addAll method is implemented as a sequence of addFirst invocations on the backing deque.

	Queue<String> q= Collections.asLifoQueue(Dq);
	
	//search using binary search
	Collections.binarySearch(list, "d");
	
	//returns as checked collection as given in second argument.
	//list ,map and set can obtained as checked using checkedList(),checkedMap,
	//checkedSet can be retrieved using this utility.
	Collections.checkedCollection(list, String.class);
	
	//used for sorted checked maps
	Collections.checkedSortedMap(smap, String.class, String.class);
	
	//used for checked sorted set
	Collections.checkedSortedSet(sset, String.class);
	
	//used for copying from one collection to second collection 
	Collections.copy(destinationList, sourceList);
	
	//Returns true if the two specified collections have no elements in common. 
	Collections.disjoint(destinationList, sourceList);
	
	//fill the specified collection with given second argument
	Collections.fill(list, "filledData");
	
	//finds occurrence of specified element 
	Collections.frequency(list,"a");
	
	/*Returns the starting position of the first occurrence of the specified target list 
	within the specified source list, or -1 if there is no such occurrence. More formally,
	returns the lowest index i such that source.subList(i, i+target.size()).equals(target),
	or -1 if there is no such index. (Returns -1 if target.size() > source.size().) */
int indexOfSubList = Collections.indexOfSubList(sourceList, destinationList);
int lastIndexOfSubList = Collections.lastIndexOfSubList(sourceList, destinationList);

  //returns list of specified enumeration elements
   list = Collections.list(en);
   
   //returns max of specified element which are comparable according to compareTo() method.
   //It can take comparator as second argument and sort according to given comparator. 
   Collections.max(list);
   
   //returns min value of specified collection and can also take second argument as comparator.
   Collections.min(list);
   
   //returns immutable list of object specified as no of copies as inputed. 
   list = Collections.nCopies(100, "abc");
   
   //get set from specified map 
 Set set =  Collections.newSetFromMap(stMap);
 //Collections.newSetFromMap(smap);
  //replaces all occurrence of elements of collections with new elements
  Collections.replaceAll(list, "a", "");
  
  //returns reverse order of specified collection.
  Collections.reverse(list);
  
  //returns comparator of natural order sorting.
  Collections.reverseOrder();
 
  //returns comparator of specified comparator if cmp is null method is equivalent to Collections.reverseOrder().
  Comparator<String> cmp = null;
  Collections.reverseOrder(cmp);
  
  //rotates specified collection from 
  int distance=1;
  Collections.rotate(list, distance);
  
  //shuffles order of elements
  Collections.shuffle(list);
  
  //returns singleton of specified collection or object.
  Collections.singleton(list);
  
  //sort in natural order of specified collection.
  Collections.sort(list);
  
  //sorts list according to comparator provided.
  Collections.sort(list, cmp);
  
  //swap collection i position data with j position data ,if same position list remains same
  int i = 1,j=0;
  Collections.swap(list, i, j);
  
  //get synchronized collection eigter set,map or list
  Collections.synchronizedCollection(list);
  
 //get synchronized sorted map 
  Collections.synchronizedSortedMap(smap);
  
  //get synchronized sorted set 
  Collections.synchronizedSortedSet(sset);
  
  //get unmodifiable collection either set ,map or list
  //throws UnsupportedOperationException while trying to modify the collection.
  Collections.unmodifiableCollection(list);
  Collections.unmodifiableList(list);
  Collections.unmodifiableMap(map);
  Collections.unmodifiableSet(set);
  //sorted set and map can be get using these methods
  Collections.unmodifiableSortedMap(smap);
  Collections.unmodifiableSortedSet(sset);
	}
	
@SuppressWarnings({ "unchecked", "rawtypes" })
public static void main(String[] args) {

System.out.println("++++++++++++++++++++++++++++++++++++++++++Uses of Collections class methods+++++++++++++++");

List list =new ArrayList<Object>();
list.add(1);
list.add(2);
list.add(3);
list.add(4);
list.add(5);
list.add(6);
Object obj =new Object();
//obj.
try{
	
Collections.singletonList(obj);
list = Collections.singletonList(list);
System.out.println("singleton ::"+Collections.singleton(list));
System.out.println("frequency of null::"+Collections.frequency(list,null));
//Collections.addAll(list, "String","String","String","String","String");
System.out.println("Frequency of String:: "+Collections.frequency(list,"String"));
List objectCopies= Collections.nCopies(4, obj);
List fillList = new ArrayList<Object>();
  Collections.fill(fillList, obj);
  String [] strArr = new String[10];
 List arrList =  Arrays.asList(strArr);
System.out.println(arrList);
 for(Object object:objectCopies){
	System.out.println("recent object ::"+object);
}
for(Object object:fillList){
	System.out.println("recent object filled::"+object);
}
}
catch(UnsupportedOperationException e){
	System.err.println("Eror occured .....while list modification ...modification is not allowed to list......");	
}
System.out.println("total list items are ::"+list.size());
List<String> unModList=	Collections.unmodifiableList(list);
try{
 for(String str:unModList){
	System.out.println("unmodifiable list ::"+str);
	unModList.add("notAdded");
 } 
}catch(UnsupportedOperationException ex){
	System.err.println("Eror occured .....while list modification ...modification is not allowed to list......");
}
catch (Exception e) {
  System.err.println("something went wrong....");
}
System.out.println("total items in list are..."+unModList.size());

 ConcurrentHashMap concur = new ConcurrentHashMap();
 
 Set concurset = Collections.newSetFromMap(concur);
 
 concurset.retainAll(concur.keySet());
 concur.put("a1", "S1");
 concur.put("a2", "S2");
 concur.put("a3", "S3");
 concur.put("a4", "S4");
 concur.put("a5", "S5");
 System.out.println("total concur set items ::"+concur.size());
// Set concurset = Collections.newSetFromMap(concur);
 for(Object s:concurset.toArray()){
	 System.out.println("set items are :: "+s.toString());
  }
 }

}
