package com.test.collections;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.WeakHashMap;
public class Maps {

	/**
	 * Stores key value pairs.
	 */
	
	private static Map<String,String> getData(){
		HashMap<String,String> map =new HashMap<String, String>();	
		map.put(null,null);
		map.put("Shiva", "shiva");
		map.put("ABC", "ABC");
		return map;
	}

	/**
	 * Stores key value pairs in sorted order of key.
	 */
	private static Map<String,String> getTreeMapData(){
		Object obj =new Object();
		TreeMap<String,String> map =new TreeMap<String, String>();
		map.put("Shiva", "shiva");
		map.put("ABC", "ABC");
		map.put("Shiva", "shiva");
		map.put("ABC", "ABC");
		return map;
	}
	
	/**
	 * stores reference as weak reference as if key is made null and after 
	 * calling system.gc(),it have no reference in map
	 */
	private static void getWeakMapData(){
		Object key = new Object();
		Object key1 = new Object();
	    WeakHashMap<Object,Object> m = new WeakHashMap<Object,Object>();
	    
	    HashMap<Object,Object> hmap = new HashMap<Object,Object>();
	    m.put(key, 1);
	    hmap.put(key1, 1);
	    System.out.println("total items before nullifying key ::"+m.size());
	    System.out.println("total items before nullifying key hmap::"+hmap.size());
	    key = null;
	    System.gc();
	    try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    System.out.println("total items after nullifying key ::"+m.size());
	    System.out.println("total items after nullifying key hmap::"+hmap.size());
	}
	
	
	/**
	 * stores key value pairs in insertion order of entries to map.
	 */
	private static LinkedHashMap<String, String> getLinkedMapData(){
		LinkedHashMap<String, String> linkedMap=new LinkedHashMap<String, String>();
		linkedMap.put("1", "S1");
		linkedMap.put("4", "S4");
		linkedMap.put("6", "S6");
		linkedMap.put("5", "S5");
		linkedMap.put("3", "S3");
		linkedMap.put("2", "S2");
		return linkedMap;
	}
	/**
	 * stores key value pairs using == (reference check ) for key comparison in put method
	 */
	private static Map<String,String> getIdentityHashMap(){
	    IdentityHashMap<String,String> map =new IdentityHashMap<String, String>();
		map.put("1", "shiva");
		map.put("1", "ABC");
		map.put("6", "shiva");
		map.put("8", "ABC");
		map.put(null, "test");
		map.put("", null);
		return map;
	}
	 enum STATE{
        NEW, RUNNING, WAITING, FINISHED;
    }
	/**
	 * stores enum key as key and stores key value pairs.
	 */
	private static EnumMap<STATE, String> getEnumMap(){
		EnumMap<STATE, String> enMap =new EnumMap<STATE, String>(STATE.class);
		enMap.put(STATE.NEW, "NEW");
		enMap.put(STATE.RUNNING, "RUNNING");
		enMap.put(STATE.WAITING, "WAITING");
		enMap.put(STATE.FINISHED, "finished");
		return enMap;
	}
	
	private static AbstractMap<String,String> getAbstractMap(){
		AbstractMap<String,String> absMap =new HashMap<String,String>();
		absMap.put("shiva", "shiva");
		return absMap;
	}
	
	public static void main(String[] args) {
		
		//System.out.println("verifing ............identity hashmap...........");
		
		System.out.println("verifing ............Weak hashmap...........");
		getWeakMapData();
		System.out.println("verification  ............Weak hashmap...........Done");
	    Map<String,String> map=	getLinkedMapData();
		SortedMap<String, String> sort=new TreeMap<String, String>();
		//Map<String,String> unModMap=	Collections.unmodifiableMap(map);
		Map<String,String> unModMap=	Collections.emptyMap();
		Arrays.sort(map.values().toArray());
		//unModMap.put("1", "1");
		//Collections.sort((List)map.values());
		for(String str:unModMap.keySet()){
			System.out.println("unmodifiable list ::"+str);
			//unModMap.put("hello",null);
		}
		HashMap<String,String> map1 = new HashMap<String, String>();
		for(String str :getLinkedMapData().keySet()){
			System.out.println("Linked key :: "+str);
			map1.put(str, getLinkedMapData().get(str));
			//getAbstractMap().put("new", "new");
			//getAbstractMap().remove(str);
		}	
		
		for(String str:map1.keySet()){
			System.out.println("hash map key ::"+str);
		}
		for(String str : getAbstractMap().keySet()){
			System.out.println("keys are as :: "+str);
			getAbstractMap().put("new", "new");
			//getAbstractMap().remove(str);
		}
		
		Iterator<STATE> enumKeySet = getEnumMap().keySet().iterator();
        while(enumKeySet.hasNext()){
            STATE currentState = enumKeySet.next();
            System.out.println("key : " + currentState + " value : " + getEnumMap().get(currentState));
        }
		System.out.println("identity hash map size : :"+getIdentityHashMap().size());
		//Map linkedData = ;	 
	}
}
