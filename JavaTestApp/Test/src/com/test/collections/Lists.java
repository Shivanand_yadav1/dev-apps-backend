package com.test.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Queue;

import com.test.ImmutableEmp;

public class Lists {

	/**
	 * Maintains insertion order of data. and can be retrieved indexed based .
	 * Insertions are costly at particular location of list ,as it shifts the list data and reordering of indexes happen.
	 */
	
	public static List<String> getListData(){
		List<String> strList=new ArrayList<String>();
		strList.iterator();
		strList.add("a");
		strList.add("s");
		strList.add("d");
		strList.add("a");
		strList.add("s");
		strList.add("d");
		strList.add("a");
		strList.add("s");
		strList.add("d");	
		strList.add(2, "p");
		return strList;
	}
	/**
	 * data modifications are of lesser costly as compare to arraylist as it stores references and they are shifted
	 *  but reordering is not required for data.
	 */
	public static LinkedList<String> getLinkedData(){
		LinkedList<String> linkedList = new LinkedList<String>();
		linkedList.add("aa");
		linkedList.removeFirst();
		linkedList.addFirst("ee");
		linkedList.add("qq");
		linkedList.offer("11");
		linkedList.add(3, "yy");
		linkedList.peek();
		System.out.println("first element::"+linkedList.element());
		linkedList.poll();
		String str = linkedList.peekFirst();
		System.out.println("peek first::"+str);
		linkedList.pollFirst();
		return linkedList;
	}
	public static void main(String[] args) {
	
		Queue<String> q=getLinkedData();
		System.out.println("Linked list items............ ");
		for(String itm:q){
			System.out.println("item   9225605631::"+itm);
		}
 	}
}
