package com.test;

public final class ImmutableEmp {
	
	private final String PANnumber;
	private final int phnoeNumber;
	
	public ImmutableEmp(final String panNumber,final int phNo){
		this.PANnumber=panNumber;
		this.phnoeNumber=phNo;
	}
	
	public int getPhnoeNumber() {
		return phnoeNumber;
	}
    
	public  String getPanNumber(){
	return PANnumber;
  }
	
	public static void main(String args[]){
		ImmutableEmp ImmutableEmp=new ImmutableEmp("12322adds",1);
		System.out.println("PAN no ::"+ImmutableEmp.getPanNumber());
	}

}
