package com.test;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;



public class Test {

	/**
	 * @param args
	 */
	/**
	 * Validating time HH:mm format 
	 * 
	 * @return String
	 * @throws Exception
	 */
	private static boolean isValidTime(String timeVal) {
		boolean status=false;
		if(null!=timeVal && timeVal.length()==5 && timeVal.contains(":") && !timeVal.contains("-")){
			try{
				String[] hmsArray = timeVal.split(":");	
				String HoursVal=hmsArray[0];
				String minutesVal=hmsArray[1];
			/*	 System.out.println("HoursVal"+HoursVal);
				 System.out.println("HoursVal"+minutesVal);
				 System.out.println("hmsArray[0]"+hmsArray[0]);
				 System.out.println("hmsArray[1]"+hmsArray[1]);*/
				if(HoursVal.length()==2 && minutesVal.length()==2){
					 int hours = Integer.parseInt(hmsArray[0]);
					// System.out.println("hours"+hours);
					 int minutes = Integer.parseInt(hmsArray[1]);
						//System.out.println("minutes"+minutes);
					 if(hours<=23 && minutes<=59){
						 status= true; 
					  }
				   }
			}catch(Exception e){
				
				 status= false; 
			}
			    
		}
		return status;
    }
	
	public static boolean isValidTime2(String dateStr) {
		try{
			DateFormat format = new SimpleDateFormat("HH:mm");
			Date date = format.parse(dateStr);
			
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	
	public static boolean isValidTimeStamp(Timestamp dateStr) {
		try{
			java.util.GregorianCalendar calDate = new java.util.GregorianCalendar();    
		    java.sql.Date date = new java.sql.Date(dateStr.getTime());
			calDate.setTime(date);			
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	
	private static Timestamp stringToTime(String time){
		Timestamp ts=null;
		String timeUpdated;
		if(null!=time && !time.contains("-") ){
			 timeUpdated="1970-12-01 "+ time.trim() + ":00.000000000";
				 if (null !=timeUpdated) {
						try{
						 ts=Timestamp.valueOf(timeUpdated);
						}
						catch(Exception e){
							return null;	
						}
				 }
				
				
		  }
		   return ts;		 
		}
	static{
		System.out.println("Hello....................");
	}
	public static  void testNew(){
		System.out.println("testing.................");
		String s="shiva";
		String s1="shiva";
		String s2=s;
		String s3=new String("shiva");
		System.out.println(s==s1);
		System.out.println(s1==s2);
		System.out.println(s==s2);
		System.out.println(s1==s3);
		System.out.println(s==s3);
		
		/*testing.................
		true
		true
		true
		false
		false*/
	}
	

	 public static double round(double val, int places) {
		    long factor = (long) Math.pow(10, places);

		    // Shift the decimal the correct number of places
		    // to the right.
		    val = val * factor;

		    // Round to the nearest integer.
		    long tmp = Math.round(val);

		    // Shift the decimal the correct number of places
		    // back to the left.
		    return (double) tmp / factor;
		  }

		  /**
		   * Round a double value to a specified number of decimal
		   * places.
		   *
		   * @param val    the value to be rounded.
		   * @return val rounded to places decimal places.
		   */
		  public static Double roundAmount(Double val) {
		    if (val == null)
		      return val;

		    return new Double(roundAmount(val.doubleValue()));
		  }

		  /**
		   * Round a double value to a specified number of decimal
		   * places.
		   *
		   * @param val    the value to be rounded.
		   * @return val rounded to places decimal places.
		   */
		  public static double roundAmount(double val) {
		    return round(val, 2);
		  }
		  
		  public static double roundTwoDecimals(double d) {
		  	DecimalFormat twoDForm = new DecimalFormat("#.##");
		  	return Double.valueOf(twoDForm.format(d));
			}

			public static String roundTwoDecimalsWithZero(double d) {
				DecimalFormat twoDForm = new DecimalFormat("0.00");
				return twoDForm.format(d);
			}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*System.out.println("with 12"+isValidTime("12"));
		System.out.println("with 0"+isValidTime("0"));
		System.out.println("with 12:"+isValidTime("12:"));
		System.out.println("with :00"+isValidTime(":00"));
		System.out.println("with 12:00"+isValidTime("12:00"));
		System.out.println("with 00:00"+isValidTime("00:00"));
		System.out.println("with 24:00"+isValidTime("24:00"));
		System.out.println("with 44:00"+isValidTime("44:00"));
		System.out.println("with 22:00"+isValidTime("22:00"));
		
		
		System.out.println("with 22:00"+stringToTime("22"));
		String from="23:00";
		String to="11:00";
		System.out.println("from and to"+stringToTime(to).before(stringToTime(from)));
		System.out.println("with -24:00"+isValidTime("-4:00"));
		System.out.println("with 04:-0"+isValidTime("04:-0"));
		System.out.println("with 04:00"+isValidTime("04:00"));
		System.out.println("with _4:00"+isValidTime("_4:00"));
		System.out.println("with 04:_0"+isValidTime("04:_0"));
		System.out.println("from and to"+stringToTime(null));
		System.out.println("from and to"+stringToTime("-1:00"));
		System.out.println("from and to"+stringToTime("01:_0"));
		System.out.println("from and to"+stringToTime(""));*/
		//System.out.println("from and to"+ testNew());
		/*System.out.println("format 2 time validation0"+isValidTime2("0:0"));
		System.out.println("format 2 time validation1"+isValidTime2("0:00"));
		System.out.println("format 2 time validation2"+isValidTime2("0:10"));
		System.out.println("format 2 time validation3"+isValidTime2("10:0"));
		System.out.println("format 2 time validation4"+isValidTime2("-0:00"));
		System.out.println("format 2 time validation5"+isValidTime2("_0:10"));
		System.out.println("format 2 time validation6"+isValidTime2("10:-0"));
		System.out.println("format 1 time validation0"+isValidTime("0:0"));
		System.out.println("format 1 time validation1"+isValidTime("0:00"));
		System.out.println("format 1 time validation2"+isValidTime("-0:10"));
		System.out.println("format 1 time validation3"+isValidTime("10:_0"));
		System.out.println("format 1 time validation4"+isValidTime("-0:00"));
		System.out.println("format 1 time validation5"+isValidTime("_0:10"));
		System.out.println("format 1 time validation6"+isValidTime("10:-0"));*/
		testNew();
		
		TestIntfc testIntfc= new  TestIntrfc1();
		testIntfc.abc();
		testIntfc.add();
		testIntfc.test();
		
		TestIntfc testIntfc2= new  TestInf2();
		testIntfc2.abc();
		testIntfc2.add();
		testIntfc2.test();
		Double testData1=12.124+124.455;
		Double testData2=12.124+124.455;
		Double testData3=12.124+124.455;
		Double testData=testData1+testData2+testData3;
		Double testDataVal=testData+12.158;
		//Double testData=12.124+124.455;
		System.out.println("rounded result000   ...."+roundAmount(testData));
		System.out.println("rounded resul111t   "+testData);
		
		System.out.println("New changes.....................................");
		System.out.println("rounded result222 ..."+roundAmount(testDataVal));

		System.out.println("rounded result3333.."+testDataVal);
	}
	
	
}
