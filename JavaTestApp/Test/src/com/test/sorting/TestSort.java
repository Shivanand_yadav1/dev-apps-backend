package com.test.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TestSort {

	
	public static void sortA(){
		List<A> alist = new ArrayList<A>();
		A a1 = new A(1, "shiva", "UN", 10);
		A a2 = new A(8, "Amit", "UN", 8);
		A a3 = new A(10, "Anil", "UN", 10);
		A a4 = new A(01, "babu", "UN", 7);
		A a5 = new A(10, "rahul", "UN", 9);
		A a6 = new A(18, "kamal", "UN", 20);
		A a7 = new A(11, "sakil", "UN", 5);
		alist.add(a1);
		alist.add(a2);
		alist.add(a3);
		alist.add(a4);
		alist.add(a5);
		alist.add(a6);
		alist.add(a7);
		//Collections.sort(alist);
		System.out.println("in comparable.............");
		for (A a : alist) {

			System.out.println(a.getRollNo() + "::" + a.getName() + "::"
					+ a.getAge());
		}
		Collections.sort(alist, A.ageComparator);
		System.out.println("in coparator...age.........");
		for (A a : alist) {
			System.out.println(a.getRollNo() + "::" + a.getName() + "::"
					+ a.getAge());
		}

		Collections.sort(alist, A.nameComparator);
		System.out.println("in coparator......name.......");
		for (A a : alist) {
			System.out.println(a.getRollNo() + "::" + a.getName() + "::"
					+ a.getAge());
		}

		Collections.sort(alist, A.rollNoComparator);
		System.out.println("in coparator.....rollno........");
		for (A a : alist) {

			System.out.println(a.getRollNo() + "::" + a.getName() + "::"
					+ a.getAge());
		}
		
		Arrays.sort(alist.toArray());
		System.out.println("232121....rollno........");
		for (A a : alist) {

			System.out.println(a.getRollNo() + ":4323344234234:" + a.getName() + "::"
					+ a.getAge());
		}
		List data =new ArrayList();
		data.add(1);
		data.add(2);
		data.add(3);
		data.add(8);
		data.add(5);
		data.add(6);
		data.add(7);
		data.add(9);
	//	Arrays.sort(data.toArray());
		Collections.sort(data);

		for(Object index:data){
			System.out.println("index::"+index);
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		sortA();
	}

}
