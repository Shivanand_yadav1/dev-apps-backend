package com.test.sorting;

import java.util.Comparator;

public class A implements Comparable<A> {

	public int rollNo;
	public String name;
	public String university;

	public int age;

	public int getAge() {
		return age;
	}

	public int getRollNo() {
		return rollNo;
	}

	public String getName() {
		return name;
	}

	public String getUniversity() {
		return university;
	}

	public A(int rollNo, String name, String university, int age) {
		this.rollNo = rollNo;
		this.name = name;
		this.university = university;
		this.age = age;
	}

	@Override
	public int compareTo(A e1) {
		return this.getName().compareTo(e1.getName());
		//return (this.rollNo - e1.rollNo);
	}

	public static Comparator<A> rollNoComparator = new Comparator<A>() {

		@Override
		public int compare(A e1, A e2) {
			return (int) (e1.getRollNo() - e2.getRollNo());
		}
	};

	public static Comparator<A> nameComparator = new Comparator<A>() {

		@Override
		public int compare(A e1, A e2) {
			return e1.getName().compareTo(e2.getName());
		}
	};

	public static Comparator<A> ageComparator = new Comparator<A>() {

		@Override
		public int compare(A e1, A e2) {
			return (int) (e1.getAge() - e2.getAge());
		}
	};

}
