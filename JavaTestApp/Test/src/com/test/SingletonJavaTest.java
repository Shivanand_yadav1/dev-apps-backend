package com.test;

import java.util.HashMap;
import java.util.Map;

public class SingletonJavaTest {

	/**
	 * @param args
	 * @throws CloneNotSupportedException 
	 */
	public static void main(String[] args) throws CloneNotSupportedException {
		// TODO Auto-generated method stub
	//	SingletonJava obj=new SingletonJava();//not able to instatiate
		SingletonJava obj=SingletonJava.getSigleTon(); 
		//obj.Clone();
		System.out.println("first object"+obj.toString());
		SingletonJava ob2=SingletonJava.getSigleTon(); 
		System.out.println("first object"+ob2.toString());
		Map m=new HashMap();
		m.put(obj, 1);
		m.put(ob2, 2);
		System.out.println(m.get(obj));
		if(obj.equals(ob2)){
			System.out.println("no new object created");
		}
		else{
			System.out.println("not a true singleton");
		}
	}

}
