package com.test;

public abstract class TestAbs {
	TestAbs(){
		System.out.println("in abstract....constructor........");	
	}
private static void test(){
	System.out.println("in abstract private method............");
  }
interface x{
	void TestAbs();
  }

private static  class T{
	T(String s){
		System.out.println("inner class called..."+s);	
	}
	
	int add(int x, int y){
		System.out.println("int method");
		return y+x;	
	}
	int add(Integer x, Integer y){
		System.out.println("Integer method");
		return y+x;	
	}
  }

public static void main(String[] args) {
	// TODO Auto-generated method stub
	try{
	  //	TestAbs tst=new TestAbs();
	}
  catch(Exception e){
	   System.out.println(e);  
  }
      System.out.println("normal flow now");
      test();
      System.out.println("PENDING:"+TestEnum.PENDING.getStatusCode());
      System.out.println("ACTIVE:"+TestEnum.ACTIVE.getStatusCode());
      System.out.println("DELETED:"+TestEnum.DELETED.getStatusCode());
      T t=new T("hi");
      System.out.println("sum is::"+t.add(4,5));
  }
}

