package com.test;

public enum TestEnum {
	PENDING("P"), ACTIVE("A"), INACTIVE("I"), DELETED("D");
	 
	private String statusCode;
 
	private TestEnum(String s) {
		statusCode = s;
	}
 
	public String getStatusCode() {
		return statusCode;
	}
}
