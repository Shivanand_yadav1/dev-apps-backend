package com.test;

public class Tiger {
	private String color;
	private String stripePattern;
	private int height;

	public Tiger(String color, String stripePattern, int height) {
		this.color = color;
		this.stripePattern = stripePattern;
		this.height = height;

	}

	public String getColor() {
		return color;
	}

	public int getHeight() {
		return height;
	}

	public String getStripePattern() {
		return stripePattern;
	}

	@Override
	public boolean equals(Object object) {
		boolean result = false;
		if (object == null || object.getClass() != getClass()) {
			result = false;
		} else {
			Tiger tiger = (Tiger) object;
			if (this.color == tiger.getColor()
					&& this.stripePattern == tiger.getStripePattern()
					&& this.height == tiger.getHeight()) {
				result = true;
			}
		}
		return result;
	}

	// just omitted null checks
	@Override
	public int hashCode() {
		int hash = 3;
		hash = 7 * hash + this.color.hashCode();
		hash = 7 * hash + this.stripePattern.hashCode();
		hash = 7 * hash + this.height;
		return hash;
	}

	public static void main(String args[]) {
		Tiger bengalTiger1 = new Tiger("Yellow", "Dense", 3);
		Tiger bengalTiger2 = new Tiger("Yellow", "Dense", 3);
		Tiger siberianTiger = new Tiger("White", "Sparse", 4);
		System.out.println("bengalTiger1 and bengalTiger2: "
				+ bengalTiger1.equals(bengalTiger2));
		System.out.println("bengalTiger1 and siberianTiger: "
				+ bengalTiger1.equals(siberianTiger));
		if (bengalTiger1 == bengalTiger2) {
			System.out.println("both are same for ==");
		} else {
			System.out.println("both are not same ==");
		}

		if (bengalTiger1.equals(bengalTiger2)) {
			System.out.println("both are same for equals");
		}
		else {
			System.out.println("both are not equal for equals");
		}
	}

}
