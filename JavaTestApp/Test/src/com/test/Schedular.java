package com.test;

import java.util.Timer;
import java.util.TimerTask;

import com.test.sorting.TestSort;

/**
 * Simple demo that uses java.util.Timer to schedule a task 
 * to execute once 5 seconds have passed.
 */

public class Schedular {
    Timer timer;

    public Schedular(int seconds) {
        timer = new Timer();
        timer.schedule(new RemindTask(), seconds*1000,6000);
        //timer.schedule(new RemindTask(), seconds*1000);
	}

    class RemindTask extends TimerTask {
        public void run() {
            System.out.println("hello Shiva!");
            TestSort.sortA();
        //  timer.cancel(); //Terminate the timer thread
        }
    }

    public static void main(String args[]) {
        new Schedular(5);
        //System.out.println("Task scheduled.");
       
    }
}

