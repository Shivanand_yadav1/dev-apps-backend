package com.inners;

import java.util.Arrays;
import java.util.Collections;

class Outer_Demo1{
	   int num;
	   static int numStatic;
	   //inner class
	   private class Inner_Demo{
	      public void print(){	   
	         System.out.println("This is an inner class");
	         System.out.println("parent data::"+num);
		      System.out.println("parent data numStatic::"+numStatic);
	      }
	   }
	   //Accessing he inner class from the method within
	   void display_Inner(){
	      Inner_Demo inner = new Inner_Demo();
	      inner.print();
	/*      System.out.println(parent data::+num);
	      System.out.println(parent data numStatic::+numStatic);*/
	   }
	}
	   
	public class Outer_Demo{
	   public static void main(String args[]){
	      //Instantiating the outer class 
		   Outer_Demo1 outer = new Outer_Demo1();
	      //Accessing the display_Inner() method.
	      outer.display_Inner();
	      
	      int[] arr=new int[10];
	      arr[0] = 0;
	      arr[1] = 1;
	      arr[2] = 2;
	      arr[3] = 3;
	      arr[4] = 4;
	      arr[5] = 5;
	      arr[6] = 6;
	      int[] arr1=new int[10];
	      arr1[0] = 0;
	      arr1[1] = 1;
	      arr1[2] = 2;
	      arr1[3] = 3;
	      arr1[4] = 4;
	      arr1[5] = 5;
	      arr1[6] = 6;
	      System.out.println("are both arrays equal :: "+Arrays.equals(arr, arr1));
	    //  System.out.println("are both arrays equal :: "+Collections.disjoint(arr, arr1));
	      
	   }

	}
