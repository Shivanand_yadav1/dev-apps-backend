
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

public class Main {
	
	public static String convertCurrentTimeToZoneTime(String timeZone){
		 Date date = new Date();
		 SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss Z");
		 df.setTimeZone(TimeZone.getTimeZone(timeZone));
		return df.format(date);
		
	}

	public static Timestamp convertDeviceTimeToTimestamp(String str_date) {
	    try {
	    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	    	SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	Date d =  sdf.parse(str_date);
	    	String formattedTime = output.format(d);
	    	return  getDateFromString(formattedTime);
	    } catch (ParseException e) {
	      System.out.println("Exception :" + e);
	      return null;
	    }
	  }
	public static Timestamp getDateFromString(String dateStr)
			throws ParseException {
	
		Timestamp ts = null;
		
		if (null != dateStr) {
			try {
				ts = Timestamp.valueOf(dateStr);
			} catch (Exception e) {
				return null;
			}
		}
		return ts;
	}
	private static String[] getData(){
		String[] strArr = new String[10];
		for(int i =0;i<10;i++){
			strArr[i] = String.valueOf(i);
		}
		return strArr;
	}
	private static void testLoop(){
		for(int i=0;i<10;i++){
			i++;
			System.out.println(i);
		 System.out.println(i+1);
		 i++;
		}
	}
	
	private static boolean isOdd(int num){	
			return num%2==1;
		
	}
	private static boolean isEven(int num){	
		return num%2==0;
	
}
  public static void main(String...strings) {
	  testLoop();
	 System.out.println("Is odd"+isOdd(10));
	 System.out.println("Is even"+isEven(10));
   /* SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
    System.out.println(f.format(new Date()));
    
    SimpleDateFormat f2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
    System.out.println(f2.format(new Date()));*/
	  
	//  Date date = new Date();
	 System.out.println("Arr data::::::::::::::::::"+Arrays.deepToString(getData())); ;
	  
//	  SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy HH:mm:ss Z");
	 // SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss Z");
     // 2015-07-29T00:17:28.534 -0400
	  //2015-07-29T00:19:32.032 -0400
	  //2015-07-23T07:34:49.624Z
	  //"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"; 
	  //dd-MMM-yy HH:mm:ss
	  String timeZone="America/New_York";
	  //America time zones tme_zone table timezone_id
	  //select * from time_zone
	//  select city_id , name,time_zone_id from city where time_zone_id like( '%America%') and name like '%Emb%'
	  
	  /*America/Anchorage
	  America/Barbados
	  America/Bogota
	  America/Buenos_Aires
	  America/Campo_Grande
	  America/Cayman
	  America/Chicago
	  America/Cordoba
	  America/Costa_Rica
	  America/Cuiaba
	  America/Denver
	  America/Edmonton
	  America/Fortaleza
	  America/Halifax
	  America/Indianapolis
	  America/Lima
	  America/Los_Angeles
	  America/Maceio
	  America/Managua
	  America/Manaus
	  America/Mexico_City
	  America/Montevideo
	  America/Montreal
	  America/Nassau
	  America/New_York
	  America/Panama
	  America/Phoenix
	  America/Port_of_Spain
	  America/Puerto_Rico
	  America/Recife
	  America/Regina
	  America/Santiago
	  America/Sao_Paulo
	  America/St_Johns
	  America/St_Thomas
	  America/Vancouver
	  America/Venezuela
	  America/Winnipeg
	  American/Cayman*/
/*	  Integer i1=0;
	  // Use Madrid's time zone to format the date in
	  df.setTimeZone(TimeZone.getTimeZone(timeZone));
	  System.out.println("Date and time in timeZone "+timeZone+"  :::  " +( (convertDeviceTimeToTimestamp("2016-01-26T14:09:04-0400").getTime()-convertDeviceTimeToTimestamp("2016-01-27T14:12:59+0530").getTime())/1000));
	  Integer i = 1;
	  boolean test = i!=null && i == 0;
	  System.out.println("test"+test);
	  System.out.println("Date and time in timeZone "+timeZone+"  :::  " +i1.toString());
	  //System.out.println("new"+convertCurrentTimeToZoneTime("America/New_York"));
	  
	  Long timeInMillis = System.currentTimeMillis();
Map env=	  System.getenv();
Iterator itr =env.values().iterator();	

	  Long hours =(timeInMillis/1000)/(24*60 *60);
      System.out.println("hours :" + hours);
      System.out.println("key :" +itr.toString());
      System.out.println("value :" + hours);
      System.out.println("time value in millis :" + timeInMillis);
      System.out.println("time value in millis :" + env.getClass().getSimpleName());
  }
  
  public static void main(String...strings) {
	  System.out.println("Date and time in timeZone ");  
  }

  Childs chld=new Childs();*/
  }
  
}
